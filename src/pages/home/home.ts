import { Component } from '@angular/core';
import { NavController, AlertController, ItemSliding } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { SocialSharing } from '@ionic-native/social-sharing';
import { Storage } from '@ionic/storage';
import { Clipboard } from '@ionic-native/clipboard';

import { barcode } from './../../models/barcodes'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  torch: boolean = false;
  torchColor: string = "light"
  barcodes: Array<barcode> = [];
  constructor(private clipboard: Clipboard, private storage: Storage, public navCtrl: NavController, private barcodeScanner: BarcodeScanner, private socialSharing: SocialSharing, public alertCtrl: AlertController) {
    this.storage.get("barcodes").then((data: any) => {
      if (data != null) {
        console.log(data);
        this.barcodes = data;
      }
    })
  }

  scanBarCode() {
    console.log("getBarcode");
    let options = {
      showTorchButton: true
    }

    this.barcodeScanner.scan(options).then((barcodeData) => {
      // Success! Barcode data is here
      console.log(barcodeData);
      if (barcodeData.text != "") {
        this.barcodes.push({ barcode: barcodeData.text, description: "", timestamp: (new Date().toLocaleString()) })
        this.setBarcodes();
      }
    }, (err) => {
      // An error occurred
    });
  }

  barcodeClicked(barcode: barcode) {
    this.clipboard.copy(barcode.barcode);
  }

  shareMail() {
    let barcodes: string = "";
    for (let barcode of this.barcodes) {
      let index = this.barcodes.indexOf(barcode);
      if (index == 0) {
        barcodes = barcode.barcode;
      } else {
        barcodes = barcodes + "\n" + barcode.barcode;
      }
    }
    // Share via email
    /*
    this.socialSharing.shareViaEmail(barcodes, 'Subject', ['recipient@example.org']).then(() => {
      console.log("Success");
    }).catch(() => {
      console.log("Error");
    });*/
    this.socialSharing.share(barcodes, "Scanned barcodes");
  }

  removeBarcode(slidingItem: ItemSliding, barcode: barcode) {
    console.log(barcode);
    this.barcodes.splice(this.barcodes.indexOf(barcode), 1);
    this.setBarcodes();
    slidingItem.close();
  }

  shareBarcode(slidingItem: ItemSliding, barcode: barcode) {
    console.log(barcode);
    let barcodeAsString : string = "Code: " + barcode.barcode + " @" + barcode.timestamp;
    this.socialSharing.share(barcodeAsString, "Scanned barcode");
    slidingItem.close();
  }

  setBarcodes() {
    this.storage.set("barcodes", this.barcodes);
  }

  clearList() {
    const alert = this.alertCtrl.create({
      title: 'Trash',
      message: 'Delete barcodes',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            this.barcodes = [];
            this.storage.set("barcodes", "");
          }
        }
      ]
    });
    alert.present();
  }
}
